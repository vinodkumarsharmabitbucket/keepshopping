package com.example.keepshopping.listener;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.keepshopping.R;
import com.example.keepshopping.model.CheckoutController;
import com.example.keepshopping.model.CheckoutItem;
import com.example.keepshopping.model.Item;

public class AddItemListener implements View.OnClickListener{

	private int itemCounter=0;
	int counter=0;
	
	private Context context;
	private CheckoutController checkoutController;
	private CheckoutItem checkoutItem;
	private int position;
	public TextView count;
	private List<Item> dryFruitsList;
	private View view;
	private Item item;

	
	public AddItemListener(Context context,Item item,View view,int position){
		this.context=context;
		this.item=item;
		this.view=view;
		this.position=position;
		count=(TextView)view.findViewById(R.id.count);
		checkoutItem=new CheckoutItem();
		checkoutItem.setItem(item);
		checkoutController=CheckoutController.getInstance();
	}
	
	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.additem:
			++itemCounter;
			
			if(itemCounter==1){
				checkoutController.getCheckoutItems().add(checkoutItem);
			}
			count.setText(Integer.toString(itemCounter));
			checkoutItem.setNuumberOfItems(itemCounter);
			break;

		case R.id.deleteItem:
			
			
			if((--itemCounter<0)||(--itemCounter==0)){
				itemCounter=0;
				checkoutController.getCheckoutItems().remove(checkoutItem);
			}
			count.setText(Integer.toString(itemCounter));
			checkoutItem.setNuumberOfItems(itemCounter);
			counter=0;
			break;
			
		default:
			break;
		}
		
	}	

}
