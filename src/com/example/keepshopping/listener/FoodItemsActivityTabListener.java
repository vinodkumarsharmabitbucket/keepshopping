package com.example.keepshopping.listener;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.support.v4.view.ViewPager;

public class FoodItemsActivityTabListener implements ActionBar.TabListener{

	private ViewPager mViewPager;
	
	public FoodItemsActivityTabListener(ViewPager mViewPager){
		this.mViewPager=mViewPager;
	}
	
	@Override
	public void onTabSelected(ActionBar.Tab tab,FragmentTransaction fragmentTransaction) {
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,FragmentTransaction fragmentTransaction) {
	}


}
