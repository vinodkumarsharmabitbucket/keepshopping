package com.example.keepshopping.adapters;

import java.util.ArrayList;
import java.util.List;

import com.example.keepshopping.R;
import com.example.keepshopping.backup.Vegetable;
import com.example.keepshopping.listener.AddItemListener;
import com.example.keepshopping.model.Item;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListAdapter extends ArrayAdapter<Item> {

	private Activity activity;
	private AddItemListener listener;
	
	public ListAdapter(List<Item> items,Activity activity) {
		super(activity.getApplicationContext(), android.R.layout.simple_list_item_1, items);
		this.activity=activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (null == convertView) {
			convertView = activity.getLayoutInflater().inflate(R.layout.item_list_layout, null);
		}
		
		Item item = getItem(position);

		TextView vegetableName =(TextView)convertView.findViewById(R.id.itemname);
		vegetableName.setText(item.getItemName());
		
		TextView quantity =	(TextView)convertView.findViewById(R.id.quantity);
		quantity.setText("Quantity :"+item.getItemQuantity());
		
		TextView quantityleft =	(TextView)convertView.findViewById(R.id.quantityleft);
		quantityleft.setText("Quantity :"+item.getItemQuantityLeft());
		
		//TextView count =(TextView)convertView.findViewById(R.id.count);
		listener=new AddItemListener(getContext(),item,convertView,position);
	
		ImageView addItem=(ImageView)convertView.findViewById(R.id.additem);
		addItem.setOnClickListener(listener);
		
		ImageView deleteItem=(ImageView)convertView.findViewById(R.id.deleteItem);
		deleteItem.setOnClickListener(listener);
		
		return convertView;
	}
}
