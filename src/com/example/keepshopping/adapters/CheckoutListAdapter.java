package com.example.keepshopping.adapters;

import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.keepshopping.R;
import com.example.keepshopping.listener.AddItemListener;
import com.example.keepshopping.model.CheckoutItem;

public class CheckoutListAdapter extends ArrayAdapter<CheckoutItem>{

	private Activity activity;
	
	public CheckoutListAdapter(List<CheckoutItem> checkoutItems,Activity activity) {
		super(activity,android.R.layout.simple_list_item_1, checkoutItems);
		//Toast.makeText(getContext(), new Integer(checkoutItems.size()).toString(),Toast.LENGTH_SHORT).show();
		this.activity=activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// if we weren't given a view, inflate one
		if (null == convertView) {
			convertView = activity.getLayoutInflater().inflate(R.layout.checkoutfragment_layout, null);
			
		}
		
		int position1=position;
		
		// configure the view for this Crime
		CheckoutItem checkoutItem = getItem(position);
		//Toast.makeText(activity, "In checkoutlist adapter",Toast.LENGTH_SHORT).show();
		
		TextView itemquantity =	(TextView)convertView.findViewById(R.id.itemsecondLine);
		itemquantity.setText("Quantity");
		
		TextView itemName =(TextView)convertView.findViewById(R.id.itemfirstLine);
		itemName.setText(checkoutItem.getItem().getItemName());
		//Toast.makeText(getContext(), new Integer(checkoutItem.getNuumberOfItems()).toString(),Toast.LENGTH_SHORT).show();
		TextView itemCount =(TextView)convertView.findViewById(R.id.itemcount);
		itemCount.setText(new Integer(checkoutItem.getNuumberOfItems()).toString());
		
		return convertView;
	}
	
}
