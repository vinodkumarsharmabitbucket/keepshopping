package com.example.keepshopping.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.keepshopping.R;
import com.example.keepshopping.backup.Vegetable;
import com.example.keepshopping.listener.AddItemListener;
import com.example.keepshopping.model.Item;

public class MyListAdapter extends BaseAdapter {

	private Activity activity;
	private AddItemListener listener;
	private ArrayList<Item> items;
	private LayoutInflater inflater;
	
	public MyListAdapter(ArrayList<Item> items,Activity activity) {
		this.activity=activity;
		this.items=items;
		this.inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView==null){
			convertView=inflater.inflate(R.layout.item_list_layout, null);
		}
		
		Item item =(Item) getItem(position);
		listener=new AddItemListener(activity.getApplicationContext(),item,convertView,position);
		
		TextView vegetableName =(TextView)convertView.findViewById(R.id.itemname);
		vegetableName.setText(item.getItemName());
		
		TextView quantity =	(TextView)convertView.findViewById(R.id.quantity);
		quantity.setText("Quantity: "+item.getItemQuantity());
		//Toast.makeText(activity.getApplicationContext(),Integer.toString(vegetable.getQuantity()), Toast.LENGTH_SHORT).show();
		//quantity.setText("Quantity");
		
		TextView quantityLeft =	(TextView)convertView.findViewById(R.id.quantityleft);
		quantityLeft.setText("Available: "+item.getItemQuantityLeft());
		//Toast.makeText(activity.getApplicationContext(),Double.toString(vegetable.getQuantityLeft()), Toast.LENGTH_SHORT).show();
		//TextView count =(TextView)convertView.findViewById(R.id.count);
		
		ImageView addItem=(ImageView)convertView.findViewById(R.id.additem);
		addItem.setOnClickListener(listener);
		
		ImageView deleteItem=(ImageView)convertView.findViewById(R.id.deleteItem);
		deleteItem.setOnClickListener(listener);
		
		return convertView;
	}

}
