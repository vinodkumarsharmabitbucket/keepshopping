package com.example.keepshopping.adapters;

import java.util.Locale;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import android.util.Log;

import com.example.keepshopping.FoodItemsActivity;
import com.example.keepshopping.R;
import com.example.keepshopping.fragments.DryFruitsFragment;
import com.example.keepshopping.fragments.FruitVegetableFragment;
import com.example.keepshopping.fragments.GenericFragment;
import com.example.keepshopping.fragments.MilkProductsFragment;
import com.example.keepshopping.fragments.ReadytoCookFragment;
import com.example.keepshopping.fragments.VegetablesFragment;


public class SectionsPagerAdapter extends FragmentPagerAdapter {

	private Activity activity;
	private String viewPagerName;
	private int viewPagerItems;
	public static String tabName;
	public String[][] tabs;
	
	public SectionsPagerAdapter(FragmentManager fm,Activity activity) {
		super(fm);
		this.activity=activity;
		this.viewPagerName=FoodItemsActivity.VIEW_PAGER_NAME;
		this.viewPagerItems=Integer.parseInt(FoodItemsActivity.VIEW_PAGER_ITEMS);
		setViewPagerItems(viewPagerName);
	}
	
	@Override
	public Fragment getItem(int position) {
			return new GenericFragment(tabs[position][1]);
		/*if(viewPagerName.equalsIgnoreCase("fruitvegetable")){
			return new GenericFragment(tabName);
		}

		else if(viewPagerName.equalsIgnoreCase("confectionary")){
			return new MilkProductsFragment(position);
		}

		else{
			return new FruitVegetableFragment(position);
		}*/
	}

	@Override
	public int getCount() {
		return viewPagerItems;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		Locale l = Locale.getDefault();

		return tabs[position][0];
	}
	
	public void setViewPagerItems(String viewPagerName){
		if(viewPagerName.equals("fruitvegetable")){
			Log.i("http","fruitvegetable");
			tabs=new String[][]{{"VEGETABLES","vegetable"},
					{"FRUITS","fruit"},
					{"DRY FRUITS","dryfruit"},
					{"READY TO COOK","readytocook"},
					{"CHIPS AND NAMKEENS","chipsnamkeen"}};
		}
		
		else if (viewPagerName.equals("snackfoods")) {
			tabs=new String[][]{{"BISCUITS","biscuits"},
					{"CONFECTIONERY","confectionary"},
					{"CHIPS","chips"},
					{"CHOCOLATES","chocolates"},
					{"CHOCOLATES","chocolates"}};
		}
		
		else if (viewPagerName.equals("bathbody")) {
			tabs=new String[][]{{"BISCUITS","biscuits"},
					{"CONFECTIONERY","confectionary"},
					{"CHIPS","chips"},
					{"CHOCOLATES","chocolates"},
					{"CHOCOLATES","chocolates"}};
		}
		
		else if (viewPagerName.equals("breakfastdairy")) {
			tabs=new String[]{"BREAD","BUTTER","CHIPS","CHOCOLATES","CHOCOLATES"};
		}
		
		else if (viewPagerName.equals("fooddrink")) {
			tabs=new String[]{"BISCUITS","CONFECTIONERY","CHIPS","CHOCOLATES","CHOCOLATES"};
		}
		
		else if (viewPagerName.equals("homehygiene")) {
			tabs=new String[]{"BISCUITS","CONFECTIONERY","CHIPS","CHOCOLATES","CHOCOLATES"};
		}
		
		else if (viewPagerName.equals("hardware")) {
			tabs=new String[]{"BISCUITS","CONFECTIONERY","CHIPS","CHOCOLATES","CHOCOLATES"};
		}
		
		else {
			tabs=new String[][]{{"BISCUITS","biscuits"},
				{"CONFECTIONERY","confectionary"},
				{"CHIPS","chips"},
				{"CHOCOLATES","chocolates"},
				{"CHOCOLATES","chocolates"}};
		}
	}
}

