package com.example.keepshopping.parser;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import com.example.keepshopping.model.Item;


public class ItemXmlParser {

	public static ArrayList<Item> parseFeed(String content) {

		try {

			boolean inDataItemTag = false;
			String currentTagName = "";
			Item item = null;
			ArrayList<Item> itemList = new ArrayList<Item>();

			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			XmlPullParser parser = factory.newPullParser();
			parser.setInput(new StringReader(content));

			int eventType = parser.getEventType();

			while (eventType != XmlPullParser.END_DOCUMENT) {

				switch (eventType) {
				case XmlPullParser.START_TAG:
					currentTagName = parser.getName();
					if (currentTagName.equals("item")) {
						inDataItemTag = true;
						item = new Item();
						itemList.add(item);
					}
					break;

				case XmlPullParser.END_TAG:
					if (parser.getName().equals("item")) {
						inDataItemTag = false;
					}
					currentTagName = "";
					break;

				case XmlPullParser.TEXT:
					if (inDataItemTag && item != null) {
						switch (currentTagName) {
						case "itemId":
							item.setItemId(parser.getText());
							break;
						case "itemName":
							item.setItemName(parser.getText());
							break;
						case "itemPrice":
							item.setItemPrice(Double.parseDouble(parser.getText()));
							break;
						case "itemQuantity":
							item.setItemQuantity(parser.getText());
							break;
						case "itemQuantityLeft" :
							item.setItemQuantityLeft(parser.getText());
							break;
						case "itemType" :
							item.setItemType(parser.getText());
							
						default:
							break;
						}
					}
					break;
				}

				eventType = parser.next();

			}

			return itemList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} 


	}

}
