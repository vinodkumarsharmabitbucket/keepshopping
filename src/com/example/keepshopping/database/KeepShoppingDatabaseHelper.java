package com.example.keepshopping.database;

import com.example.keepshopping.backup.DryFruits;
import com.example.keepshopping.backup.Fruits;
import com.example.keepshopping.backup.ReadytoCook;
import com.example.keepshopping.backup.Vegetable;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class KeepShoppingDatabaseHelper extends SQLiteOpenHelper {

	private static final String DB_NAME = "keepshopping.sqlite";
	private static final int VERSION = 1;

	private static final String TABLE_DRYFRUITS = "dryfruits";
	private static final String TABLE_VEGETABLE = "vegetable";
	private static final String TABLE_FRUITS = "fruits";
	private static final String TABLE_READYTOCOOK = "readytocook";
	private static final String COLUMN_RUN_START_DATE = "start_date";
	private static final String TABLE_CHECKOUTITEM = "checkoutitem";
	
	private static final String COLUMN_ITEMNAME="itemName"; 
	private static final String COLUMN_ITEMINFO="itemInfo"; 
	private static final String COLUMN_QUANTITY="qunatity";
	private static final String COLUMN_QUANTITYLEFT="quantityLeft"; 
	private static final String COLUMN_PRICE="price";
	
	public KeepShoppingDatabaseHelper(Context context) {
		super(context, DB_NAME, null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
		db.execSQL("create table dryfruits (" +
				"_id integer primary key autoincrement, quantity integer, quantityLeft integer, price real, itemName text,itemInfo text)");
		db.execSQL("create table vegetable (" +
				"_id integer primary key autoincrement, quantity integer, quantityLeft integer, price real, itemName text,itemInfo text)");
		db.execSQL("create table fruits (" +
				"_id integer primary key autoincrement, quantity integer, quantityLeft integer, price real, itemName text,itemInfo text)");
		db.execSQL("create table readytocook (" +
				"_id integer primary key autoincrement, quantity integer, quantityLeft integer, price real, itemName text,itemInfo text)");
		
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Implement schema changes and data massage here when upgrading
	}

	public long insertDryFruits(DryFruits dryfruit) {
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_ITEMNAME, dryfruit.getItemName());
		cv.put(COLUMN_ITEMINFO, dryfruit.getItemInfo());
		cv.put(COLUMN_QUANTITY,dryfruit.getItemQuantity());
		cv.put(COLUMN_QUANTITYLEFT, dryfruit.getItemQuantityLeft());
		cv.put(COLUMN_PRICE, dryfruit.getItemPrice());
		return getWritableDatabase().insert(TABLE_DRYFRUITS, null, cv);
	}
	
	public long insertVegetable(Vegetable vegetable) {
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_ITEMNAME, vegetable.getItemName());
		cv.put(COLUMN_ITEMINFO, vegetable.getItemInfo());
		cv.put(COLUMN_QUANTITY,vegetable.getItemQuantity());
		cv.put(COLUMN_QUANTITYLEFT, vegetable.getItemQuantityLeft());
		cv.put(COLUMN_PRICE, vegetable.getItemPrice());
		return getWritableDatabase().insert(TABLE_DRYFRUITS, null, cv);
	}
	
	public long insertFruits(Fruits fruit) {
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_ITEMNAME, fruit.getItemName());
		cv.put(COLUMN_ITEMINFO, fruit.getItemInfo());
		cv.put(COLUMN_QUANTITY,fruit.getItemQuantity());
		cv.put(COLUMN_QUANTITYLEFT, fruit.getItemQuantityLeft());
		cv.put(COLUMN_PRICE, fruit.getItemPrice());
		return getWritableDatabase().insert(TABLE_DRYFRUITS, null, cv);
	}
	
	public long insertReadytoCook(ReadytoCook readyToCook) {
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_ITEMNAME, readyToCook.getItemName());
		cv.put(COLUMN_ITEMINFO, readyToCook.getItemInfo());
		cv.put(COLUMN_QUANTITY,readyToCook.getItemQuantity());
		cv.put(COLUMN_QUANTITYLEFT, readyToCook.getItemQuantityLeft());
		cv.put(COLUMN_PRICE, readyToCook.getItemPrice());
		return getWritableDatabase().insert(TABLE_DRYFRUITS, null, cv);
	}

}
