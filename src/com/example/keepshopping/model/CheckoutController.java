package com.example.keepshopping.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;

public class CheckoutController{

	public Context context;
	private final static  CheckoutController checkController=new CheckoutController();
	//private Map<String, ArrayList<? extends Item>> checkoutItems;
	//private List<Item> checkoutItems;
	
	private List<CheckoutItem> checkoutItems;

	private CheckoutController(){
		
		//checkoutItems=new HashMap<String,ArrayList<? extends Item>>();
		checkoutItems=new ArrayList<CheckoutItem>();
		//configureMap();
	}

	public static CheckoutController getInstance(){
		if(checkController != null){
			return checkController;
		}

		return new CheckoutController();
	}

	public List<CheckoutItem> getCheckoutItems() {
		return checkoutItems;
	}

	public void setCheckoutItems(List<CheckoutItem> checkoutItems) {
		this.checkoutItems = checkoutItems;
	}

	
/*	public List<Item> getCheckoutItems() {
		return checkoutItems;
	}

	public void setCheckoutItems(List<Item> checkoutItems) {
		this.checkoutItems = checkoutItems;
	}*/

	
	/*private void configureMap() {
		checkoutItems.put("DryFruits", new ArrayList<DryFruits>());
		checkoutItems.put("Vegetables", new ArrayList<Vegetable>());
	}
	 */



	/*public Map<String, ArrayList<? extends Item>> getCheckoutItems() {
	return checkoutItems;
}

public void setCheckoutItems(Map<String, ArrayList<? extends Item>> checkoutItems) {
	this.checkoutItems = checkoutItems;
}*/
}
