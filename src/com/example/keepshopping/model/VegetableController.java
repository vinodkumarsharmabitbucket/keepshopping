package com.example.keepshopping.model;

import java.util.ArrayList;

public class VegetableController {

	//private ArrayList<Vegetable> vegetables;
	private ArrayList<Item> vegetables;
	private final static VegetableController vegetablecontroller=new VegetableController();
	
	private VegetableController(){
		vegetables=new ArrayList<Item>();
		
		for(int i=0;i<10;i++){
			Item vegetable=new Item();
			vegetable.setItemName("Brinjal "+i);
			vegetable.setItemPrice(Math.random()*10);
			vegetable.setItemQuantity("100");
			vegetable.setItemQuantityLeft("1000");
			vegetables.add(vegetable);
		}
	}
	
	public static VegetableController getInstance(){
		if(vegetablecontroller==null){
			return new VegetableController();
		}
		
		return vegetablecontroller;
	}

	public ArrayList<Item> getVegetables() {
		return vegetables;
	}

	public void setVegetables(ArrayList<Item> vegetables) {
		this.vegetables = vegetables;
	}
	
}
