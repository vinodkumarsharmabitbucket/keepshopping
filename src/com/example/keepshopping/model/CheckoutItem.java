package com.example.keepshopping.model;

public class CheckoutItem {

	private Item item;
	private int nuumberOfItems;
	
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public int getNuumberOfItems() {
		return nuumberOfItems;
	}
	public void setNuumberOfItems(int nuumberOfItems) {
		this.nuumberOfItems = nuumberOfItems;
	}
	
	
}
