package com.example.keepshopping.model;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.keepshopping.adapters.HttpManager;
import com.example.keepshopping.fragments.GenericFragment;
import com.example.keepshopping.fragments.ItemFragment;
import com.example.keepshopping.parser.ItemXmlParser;

public class ItemController {

	public static ArrayList<Item> items;
	private Context context;
	private String itemString;
	private String collectionName;
	public String collectionContent;
	List<GenericFragment> gFragments=new ArrayList<>();

	int i=0;
	int j=0;

	private String uri="https://keepshopping-webservices.herokuapp.com/webapi/kscontrl/";
	//	private String uri="http://services.hanselandpetal.com/feeds/flowers.xml";

	public ItemController(Context context){
		this.context=context;
	}

	public void requestData(String collectionName){
		if(isOnline(context)){
			uri=uri+collectionName;
			MyAsync task=new MyAsync();
			task.execute(uri);
		}
		else{
			Toast.makeText(context, "You are not connected to Internet!!!", Toast.LENGTH_LONG).show();
		}
	}

	public boolean isOnline(Context context){
		ConnectivityManager cm=(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo nInfo=cm.getActiveNetworkInfo();
		if(nInfo.isConnectedOrConnecting()&&nInfo!=null)
			return true;
		else 
			return false;
	}

	public void addFragment(GenericFragment gFragment){
		gFragments.add(gFragment);
	}

	public ArrayList<Item> _getData(String itemType){
		ArrayList<Item> itemList=new ArrayList<Item>();
		if(items!=null){
			for(Item item : items){
				if(item.getItemType().equals(itemType)){
					itemList.add(item);
				}
			}
			Log.i("Exception", "In withoutnull.");
			return itemList;
		}
		Log.i("Exception", "In null.");
		return null;
	}	

	public void showFragmentData(){
		int size=gFragments.size();
		Log.i("http",new Integer(size).toString());
		
		for (GenericFragment genericFragment : gFragments) {
			Log.i("http1",genericFragment.tabName);
			genericFragment.updateView(_getData(genericFragment.tabName));
		}
	}

	private class MyAsync extends AsyncTask<String, String, String>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			return HttpManager.getData(params[0]);
		}

		@Override
		protected void onPostExecute(String result) {
			items=ItemXmlParser.parseFeed(result);
			Log.i("http",Integer.toString(items.size()));
			showFragmentData();
		}
	}
}
