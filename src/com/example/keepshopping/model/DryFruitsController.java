package com.example.keepshopping.model;

import java.util.ArrayList;

public class DryFruitsController {

	private ArrayList<Item> dryfruits;
	private static DryFruitsController dryfruitsController;
	
	private DryFruitsController(){
		dryfruits=new ArrayList<Item>();
		
		for(int i=0;i<10;i++){
			Item dryfruit=new Item();
			dryfruit.setItemName("Kaju "+i);
			dryfruit.setItemPrice(Math.random()*10);
			dryfruit.setItemQuantity("91");
			dryfruit.setItemQuantityLeft("1000");
			dryfruits.add(dryfruit);
		}
	}
	
	public static DryFruitsController getInstance(){
		if(dryfruitsController==null){
			return new DryFruitsController();
		}
		
		return dryfruitsController;
	}

	public ArrayList<Item> getDryFruits() {
		return dryfruits;
	}

	public void setDryFruits(ArrayList<Item> dryfruits) {
		this.dryfruits = dryfruits;
	}

}
