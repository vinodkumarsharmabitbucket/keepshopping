package com.example.keepshopping.model;

public class Item {

	private String itemName;
	private double itemPrice;
	private String itemQuantityLeft;
	private String itemQuantity;
	private String itemId;
	private String itemType;
	
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public double getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}
	public String getItemQuantityLeft() {
		return itemQuantityLeft;
	}
	public void setItemQuantityLeft(String itemQuantityLeft) {
		this.itemQuantityLeft = itemQuantityLeft;
	}
	public String getItemQuantity() {
		return itemQuantity;
	}
	public void setItemQuantity(String itemQuantity) {
		this.itemQuantity = itemQuantity;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
}
