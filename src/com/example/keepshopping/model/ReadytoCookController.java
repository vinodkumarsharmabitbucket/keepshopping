package com.example.keepshopping.model;

import java.util.ArrayList;

public class ReadytoCookController {
	
	private ArrayList<Item> readytocooks;
	private static ReadytoCookController readytocookcontroller;

	private ReadytoCookController(){
		readytocooks=new ArrayList<Item>();

		for(int i=0;i<10;i++){
			Item readytocook=new Item();
			readytocook.setItemName("Kadai Chicken "+i);
			readytocook.setItemPrice(Math.random()*10);
			readytocook.setItemQuantity("5");
			readytocook.setItemQuantityLeft("800");
			readytocooks.add(readytocook);
		}
	}

	public static ReadytoCookController getInstance(){
		if(readytocookcontroller==null){
			return new ReadytoCookController();
		}

		return readytocookcontroller;
	}

	public ArrayList<Item> getReadytoCooks() {
		return readytocooks;
	}

	public void setReadytoCooks(ArrayList<Item> readytocooks) {
		this.readytocooks = readytocooks;
	}

}
