package com.example.keepshopping.practice;

import java.util.ArrayList;
import java.util.List;

public class Car {

	String carname;
	
	public Car(String carname){
		this.carname=carname;
	}
	
	public static void main(String[] args) {
		List<Car> cars=new ArrayList<Car>();
		String[] carnames={"Skoda","Maruti","Renault","Mercedes","BMW","Volvo"};
		
		for (int i = 0; i < carnames.length ; i++) {
			cars.add(new Car(carnames[i]));
		}
		
		for (Car car : cars) {
			System.out.println("You are looking "+car.carname);
		}
	}
}
