package com.example.keepshopping.backup;

import com.example.keepshopping.model.Item;

public class DryFruits extends Item{

	private long dryFruitId;
	private String itemInfo;
	
	public DryFruits(){}

	public long getDryFruitId() {
		return dryFruitId;
	}

	public void setDryFruitId(long dryFruitId) {
		this.dryFruitId = dryFruitId;
	}

	public String getItemInfo() {
		return itemInfo;
	}

	public void setItemInfo(String itemInfo) {
		this.itemInfo = itemInfo;
	}
	
}
