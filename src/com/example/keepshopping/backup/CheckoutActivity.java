package com.example.keepshopping.backup;

import com.example.keepshopping.SingleFragmentActivity;
import com.example.keepshopping.fragments.CheckoutFragment;

import android.app.Fragment;
import android.os.Bundle;
import android.widget.Toast;

public class CheckoutActivity extends SingleFragmentActivity{

	@Override
	protected Fragment createFragment() {
		Toast.makeText(this, "CheckoutFragment created",Toast.LENGTH_SHORT).show();
		CheckoutFragment checkoutFragment=new CheckoutFragment();
		return checkoutFragment;
	}

	
}
