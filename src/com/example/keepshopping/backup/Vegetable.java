package com.example.keepshopping.backup;

import com.example.keepshopping.model.Item;

public class Vegetable extends Item{

	private long vegetableId;
	private String itemInfo;
	
	public Vegetable(){}

	public long getVegetableId() {
		return vegetableId;
	}

	public void setVegetableId(long vegetableId) {
		this.vegetableId = vegetableId;
	}
	
	public String getItemInfo() {
		return itemInfo;
	}

	public void setItemInfo(String itemInfo) {
		this.itemInfo = itemInfo;
	}
}