package com.example.keepshopping.backup;

import com.example.keepshopping.model.Item;

public class Fruits extends Item{

	private long fruitId;
	private String itemInfo;
	
	public Fruits(){}

	public long getFruitId() {
		return fruitId;
	}

	public void setFruitId(long fruitId) {
		this.fruitId = fruitId;
	}

	public String getItemInfo() {
		return itemInfo;
	}

	public void setItemInfo(String itemInfo) {
		this.itemInfo = itemInfo;
	}
}
