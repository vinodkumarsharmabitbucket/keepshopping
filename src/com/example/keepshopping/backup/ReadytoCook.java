package com.example.keepshopping.backup;

import com.example.keepshopping.model.Item;

public class ReadytoCook extends Item{

	private long readyToCookId;
	private String itemInfo;
	
	public ReadytoCook(){}

	public long getReadyToCookId() {
		return readyToCookId;
	}

	public void setReadyToCookId(long readyToCookId) {
		this.readyToCookId = readyToCookId;
	}

	public String getItemInfo() {
		return itemInfo;
	}

	public void setItemInfo(String itemInfo) {
		this.itemInfo = itemInfo;
	}
}
