package com.example.keepshopping.backup;

import java.util.ArrayList;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.keepshopping.R;
import com.example.keepshopping.listener.AddItemListener;

public class FruitsListAdapter extends ArrayAdapter<Fruits>{

	private Activity activity;
	public AddItemListener listener;
	
	public FruitsListAdapter(ArrayList<Fruits> fruits,Activity activity) {
		super(activity, android.R.layout.simple_list_item_1, fruits);
		this.activity=activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (null == convertView) {
			convertView = activity.getLayoutInflater().inflate(R.layout.item_list_layout, null);
			
		}
		
		Fruits fruit = getItem(position);

		TextView fruitName =(TextView)convertView.findViewById(R.id.itemname);
		fruitName.setText(fruit.getItemName());
		
		TextView quantity =	(TextView)convertView.findViewById(R.id.quantity);
		quantity.setText("Quantity: "+fruit.getItemQuantity());
		
		TextView quantityLeft =	(TextView)convertView.findViewById(R.id.quantityleft);
		quantityLeft.setText("Available: "+fruit.getItemQuantityLeft());
		
		//TextView count =(TextView)convertView.findViewById(R.id.count);
		listener=new AddItemListener(getContext(),fruit,convertView,position);
	
		ImageView addItem=(ImageView)convertView.findViewById(R.id.additem);
		addItem.setOnClickListener(listener);
		
		ImageView deleteItem=(ImageView)convertView.findViewById(R.id.deleteItem);
		deleteItem.setOnClickListener(listener);
		
		return convertView;
	}

}
