package com.example.keepshopping.backup;

import java.util.ArrayList;

import com.example.keepshopping.R;
import com.example.keepshopping.listener.AddItemListener;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ReadytoCookListAdapter extends ArrayAdapter<ReadytoCook> {

	private Activity activity;
	private AddItemListener listener;
	
	public ReadytoCookListAdapter(ArrayList<ReadytoCook> readytocooks,Activity activity) {
		super(activity, android.R.layout.simple_list_item_1, readytocooks);
		this.activity=activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (null == convertView) {
			convertView = activity.getLayoutInflater().inflate(R.layout.item_list_layout, null);
			
		}
		
		ReadytoCook readyToCook = getItem(position);

		TextView readyToCookName =(TextView)convertView.findViewById(R.id.itemname);
		readyToCookName.setText(readyToCook.getItemName());
		
		TextView quantity =	(TextView)convertView.findViewById(R.id.quantity);
		quantity.setText("Quantity: "+readyToCook.getItemQuantity());
		
		TextView quantityLeft =	(TextView)convertView.findViewById(R.id.quantityleft);
		quantityLeft.setText("Available: "+readyToCook.getItemQuantityLeft());
		
		//TextView count =(TextView)convertView.findViewById(R.id.count);
		listener=new AddItemListener(getContext(),readyToCook,convertView,position);
	
		ImageView addItem=(ImageView)convertView.findViewById(R.id.additem);
		addItem.setOnClickListener(listener);
		
		ImageView deleteItem=(ImageView)convertView.findViewById(R.id.deleteItem);
		deleteItem.setOnClickListener(listener);
		
		return convertView;
	}
}
