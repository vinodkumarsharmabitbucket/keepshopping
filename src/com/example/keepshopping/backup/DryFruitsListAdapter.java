package com.example.keepshopping.backup;

import java.util.ArrayList;
import java.util.Map;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.keepshopping.R;
import com.example.keepshopping.listener.AddItemListener;

public class DryFruitsListAdapter extends ArrayAdapter<DryFruits>{

	private Activity activity;
	
	public AddItemListener listener;
	
	public DryFruitsListAdapter(ArrayList<DryFruits> dryfruits,Activity activity) {
		super(activity,android.R.layout.simple_list_item_1, dryfruits);
	
		this.activity=activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (null == convertView) {
			convertView = activity.getLayoutInflater().inflate(R.layout.item_list_layout, null);
			
		}
		
		DryFruits dryfruit = getItem(position);

		TextView dryFruitName =(TextView)convertView.findViewById(R.id.itemname);
		dryFruitName.setText(dryfruit.getItemName());
		
		TextView quantity =	(TextView)convertView.findViewById(R.id.quantity);
		quantity.setText("Quantity: "+dryfruit.getItemQuantity());
		
		TextView quantityLeft =	(TextView)convertView.findViewById(R.id.quantityleft);
		quantityLeft.setText("Available: "+dryfruit.getItemQuantityLeft());
		
		TextView count =(TextView)convertView.findViewById(R.id.count);
		listener=new AddItemListener(getContext(),dryfruit,convertView,position);
	
		ImageView addItem=(ImageView)convertView.findViewById(R.id.additem);
		addItem.setOnClickListener(listener);
		
		ImageView deleteItem=(ImageView)convertView.findViewById(R.id.deleteItem);
		deleteItem.setOnClickListener(listener);
		
		return convertView;
	}
 
}


