package com.example.keepshopping.backup;

import java.util.ArrayList;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.keepshopping.R;
import com.example.keepshopping.listener.AddItemListener;

public class VegetableListAdapter_array extends ArrayAdapter<Vegetable>{

	private Activity activity;
	private AddItemListener listener;
	
	public VegetableListAdapter_array(ArrayList<Vegetable> vegetables,Activity activity) {
		super(activity, android.R.layout.simple_list_item_1, vegetables);
		this.activity=activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (null == convertView) {
			convertView = activity.getLayoutInflater().inflate(R.layout.item_list_layout, null);
			
		}
		
		Vegetable vegetable = getItem(position);

		TextView vegetableName =(TextView)convertView.findViewById(R.id.itemname);
		vegetableName.setText(vegetable.getItemName());
		
		TextView quantity =	(TextView)convertView.findViewById(R.id.quantity);
		quantity.setText("Quantity");
		
		TextView count =(TextView)convertView.findViewById(R.id.count);
		listener=new AddItemListener(getContext(),vegetable,convertView,position);
	
		ImageView addItem=(ImageView)convertView.findViewById(R.id.additem);
		addItem.setOnClickListener(listener);
		
		ImageView deleteItem=(ImageView)convertView.findViewById(R.id.deleteItem);
		deleteItem.setOnClickListener(listener);
		
		return convertView;
	}
}