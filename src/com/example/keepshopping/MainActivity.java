package com.example.keepshopping;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.keepshopping.adapters.CustomGrid;

public class MainActivity extends Activity {
	GridView grid;

	private String[][] gridItems = {
			{"Fruits Vegetables","fruitvegetable","5"},
			{"Bath and Body","bathbody","5"},
			{"Snack Foods","snackfoods","5"},
			{"Breakfast and Dairy","breakfastdairy","5"},
			{"Food and Drink","fooddrink","5"},
			{"Home and Hygiene","homehygiene","5"},
			{"Hardware","hardware","5"},
			{"Staples and Spicies","staplespices","5"}
	} ;
	
	private int[] gridImages = {
			R.drawable.ic_fresh_fruits_and_vegetables1,
			R.drawable.ic_confectionary,
			R.drawable.ic_milkproducts,
			R.drawable.ic_pharmacy,
			R.drawable.ic_hardware,
			R.drawable.ic_milkproducts,
			R.drawable.ic_milkproducts,
			R.drawable.ic_milkproducts
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		CustomGrid adapter = new CustomGrid(MainActivity.this, gridItems.length, gridImages);
		grid=(GridView)findViewById(R.id.grid);
		grid.setAdapter(adapter);
		grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
				//Toast.makeText(MainActivity.this, "You Clicked at " +web[+ position], Toast.LENGTH_SHORT).show();
				Intent startFoodItemsActivity=new Intent(MainActivity.this,FoodItemsActivity.class);
				FoodItemsActivity.VIEW_PAGER_NAME=gridItems[position][1];
				FoodItemsActivity.VIEW_PAGER_ITEMS=gridItems[position][2];
				startActivity(startFoodItemsActivity);
			}
		});

	}

}