package com.example.keepshopping.fragments;

import java.util.ArrayList;
import java.util.List;

import com.example.keepshopping.adapters.ListAdapter;
import com.example.keepshopping.adapters.MyListAdapter;
import com.example.keepshopping.backup.Vegetable;
import com.example.keepshopping.backup.VegetableListAdapter_array;
import com.example.keepshopping.model.Item;
import com.example.keepshopping.model.VegetableController;

import android.app.ListFragment;
import android.os.Bundle;

public class VegetablesFragment extends ListFragment{

	private ArrayList<Item> vegetables;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		vegetables=VegetableController.getInstance().getVegetables();
		//ArrayList<? extends Item> items=vegetables;
		ListAdapter listadapter=new ListAdapter(vegetables, getActivity());
		setListAdapter(listadapter);
	}
	
}
