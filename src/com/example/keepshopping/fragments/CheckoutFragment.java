package com.example.keepshopping.fragments;

import java.util.List;

import android.app.ListFragment;
import android.os.Bundle;
import android.widget.Toast;

import com.example.keepshopping.adapters.CheckoutListAdapter;
import com.example.keepshopping.backup.DryFruitsListAdapter;
import com.example.keepshopping.model.CheckoutController;
import com.example.keepshopping.model.CheckoutItem;
import com.example.keepshopping.model.DryFruitsController;

public class CheckoutFragment extends ListFragment {

	private List<CheckoutItem> checkoutItems;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		checkoutItems=CheckoutController.getInstance().getCheckoutItems();
		//Toast.makeText(getActivity(), "CheckoutFragment created",Toast.LENGTH_SHORT).show();
		//dryfruits=DryFruitsController.getInstance().getDryFruits();
		CheckoutListAdapter listadapter=new CheckoutListAdapter(checkoutItems, getActivity());
		setListAdapter(listadapter);
	}
	
}
