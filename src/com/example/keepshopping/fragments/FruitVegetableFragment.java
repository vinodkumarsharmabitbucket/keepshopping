package com.example.keepshopping.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.keepshopping.FoodItemsActivity;
import com.example.keepshopping.R;
import com.example.keepshopping.adapters.ListAdapter;
import com.example.keepshopping.adapters.MyListAdapter;
import com.example.keepshopping.model.DryFruitsController;
import com.example.keepshopping.model.ItemController;
import com.example.keepshopping.model.Item;
import com.example.keepshopping.model.ReadytoCookController;
import com.example.keepshopping.model.VegetableController;

public class FruitVegetableFragment extends Fragment implements ItemFragment{

	private int position;
	public String itemType=null;
	private View v;
	private TextView tv;
	private ProgressBar pb;
	private ArrayList<Item> items=new ArrayList<>();

	public FruitVegetableFragment(int position){
		this.position=position;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
	}
	@Override 
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		v=inflater.inflate(R.layout.fragments_list_view, container, false);
		pb=(ProgressBar) v.findViewById(R.id.progressBar1);
		tv=(TextView) v.findViewById(R.id.textView1);
		Log.i("http","textView Created");
		pb.setVisibility(View.VISIBLE);

		return v;
	} 

	@Override
	public void onStart(){
		super.onStart();
		
		switch (position) {
		case 0:
			prepareFragment("vegetable");
			if(items==null){
//				FoodItemsActivity.itemController.addFragment(this);
			}
			else{
				updateView(items);
			}
			break;

		case 1:
			prepareFragment("fruit");
			if(items==null){
//				FoodItemsActivity.itemController.addFragment(this);
			}
			else{
				updateView(items);
			}
			break;

		case 2:
			prepareFragment("vegetable");
			if(items==null){
//				FoodItemsActivity.itemController.addFragment(this);
			}
			else{
				updateView(items);
			}
			break;

		case 3:
			prepareFragment("vegetable");
			if(items==null){
//				FoodItemsActivity.itemController.addFragment(this);
			}
			else{
				updateView(items);
			}
			break;

		case 4:
			prepareFragment("vegetable");
			if(items==null){
//				FoodItemsActivity.itemController.addFragment(this);
			}
			else{
				updateView(items);
			}
			break;

		default:
			prepareFragment("vegetable");
			if(items==null){
//				FoodItemsActivity.itemController.addFragment(this);
			}
			else{
				updateView(items);
			}
			break;
		}
	}

	public void updateView(ArrayList<Item> items){
		StringBuilder itemNames=new StringBuilder("");
		for (Item item : items) {
			itemNames.append(item.getItemName());
		}
		Log.i("http",itemNames.toString());
		tv.setText(itemNames.toString());
		pb.setVisibility(View.INVISIBLE);
	}

	@Override
	public void prepareFragment(String itemType) {
		this.itemType=itemType;
		items=FoodItemsActivity.itemController._getData(itemType);
	}
}
