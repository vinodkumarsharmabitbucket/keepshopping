package com.example.keepshopping.fragments;

import java.util.ArrayList;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.keepshopping.adapters.ListAdapter;
import com.example.keepshopping.backup.DryFruits;
import com.example.keepshopping.backup.DryFruitsListAdapter;
import com.example.keepshopping.model.DryFruitsController;
import com.example.keepshopping.model.Item;
import com.example.keepshopping.model.VegetableController;

public class DryFruitsFragment extends ListFragment{

	private ArrayList<Item> dryFruits;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		dryFruits=DryFruitsController.getInstance().getDryFruits();
		//ArrayList<? extends Item> items=vegetables;
		ListAdapter listadapter=new ListAdapter(dryFruits, getActivity());
		setListAdapter(listadapter);
	}
	
}

