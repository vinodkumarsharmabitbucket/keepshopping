package com.example.keepshopping.fragments;

import java.util.ArrayList;

import android.app.ListFragment;
import android.os.Bundle;

import com.example.keepshopping.adapters.ListAdapter;
import com.example.keepshopping.backup.ReadytoCook;
import com.example.keepshopping.backup.ReadytoCookListAdapter;
import com.example.keepshopping.model.ItemController;
import com.example.keepshopping.model.Item;
import com.example.keepshopping.model.ReadytoCookController;

public class ReadytoCookFragment extends ListFragment {

	private ArrayList<Item> readytoCooks;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		readytoCooks=ReadytoCookController.getInstance().getReadytoCooks();
		ListAdapter listadapter=new ListAdapter(readytoCooks, getActivity());
		setListAdapter(listadapter);
	}
}
