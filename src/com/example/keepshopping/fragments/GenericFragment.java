package com.example.keepshopping.fragments;

import java.util.ArrayList;

import com.example.keepshopping.FoodItemsActivity;
import com.example.keepshopping.R;
import com.example.keepshopping.model.Item;
import com.example.keepshopping.model.ItemController;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

public class GenericFragment extends Fragment{

	public String tabName;
	View v;
	ArrayList<Item> items;
	ProgressBar bar;
	TextView tv;

	public GenericFragment(String tabName) {
		this.tabName=tabName;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		items=FoodItemsActivity.itemController._getData(tabName);
		Log.i("http",tabName);
	}

	@Override 
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		v =inflater.inflate(R.layout.fragments_list_view, container, false);
		bar=(ProgressBar) v.findViewById(R.id.progressBar1);
		tv=(TextView) v.findViewById(R.id.textView1);

		if(items==null){
			bar.setVisibility(View.VISIBLE);
			FoodItemsActivity.itemController.addFragment(this);
		}

		return v;
	} 

	@Override
	public void onStart(){
		super.onStart();	
	}

	public void updateView(ArrayList<Item> items){
		bar.setVisibility(View.INVISIBLE);
		for (Item item : items) {
			Log.i("http",item.getItemName());
			tv.append(item.getItemName());
		}
	}

}
