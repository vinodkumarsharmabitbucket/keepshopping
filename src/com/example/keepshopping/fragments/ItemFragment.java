package com.example.keepshopping.fragments;

import java.util.ArrayList;
import com.example.keepshopping.model.Item;

public interface ItemFragment{

	public void updateView(ArrayList<Item> items);	
	public void prepareFragment(String itemType);
}
